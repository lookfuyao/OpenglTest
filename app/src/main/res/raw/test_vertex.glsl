attribute vec4 position;
attribute vec3 normal;
attribute vec2 texturecoord;

varying lowp vec4 colorVarying;
varying lowp vec2 v_texCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

void main()
{
    vec3 eyeNormal = normalize(normalMatrix * normal);
    vec3 lightPosition = vec3(0.0, 0.0, 1.0);
    vec4 diffuseColor = vec4(0.4, 0.4, 1.0, 1.0);

    float nDotVP = max(0.0, dot(eyeNormal, normalize(lightPosition)));

    colorVarying = diffuseColor * nDotVP;
    v_texCoord=texturecoord;
    vec3 npos=normalize(position.xyz);//点的朝向矢量
    vec2 px=npos.xz;
    float sinthita=length(px);
    px=normalize(px);
    float thita=asin(sinthita);
    float py=2.0*sin(thita*0.5);
    v_texCoord=px*py*(1.0/pow(2.0,0.5))*0.5+0.5;
    //v_texCoord=vec2(position.x/3.0+0.5,-(position.z/3.0+0.5));

    gl_Position = modelViewProjectionMatrix * position;
}