#version 150 core
#extension GL_ARB_explicit_attrib_location : enable

//
//作为纹理坐标的位置的像素的颜色使用
//

//背景纹理
uniform sampler2D image;

//纹理坐标
in vec2 texcoord;

//片段颜色
layout (location = 0) out vec4 fc;

void main(void)
{
  //寻求像素的阴影
  fc = texture(image, texcoord);
}
