uniform sampler2D sTexture;//鱼眼纹理

void main()
{
   //根据例子改造后的代码
   vec2 pfish;
   float theta,phi,r;
   vec3 psph;

   //FOV of the fisheye, eg: 180, 220 degrees
   float FOV = 3.769911185;
   //3.141592654*2.4/2;
   //纹理图片大小
   //ivec2 size=textureSize(sTexture, 0);
   float width = 1280.0; //2440.0; //size.s;
   float height = 720.0; //2440.0; //size.t;

   // Polar angles

   //下面的代码控制左侧图片
   // -pi to pi
    theta = 2.0 * 3.14159265 * (gl_FragCoord.x / width - 0.5);
   // -pi/2 to pi/2
    phi = 3.14159265 * (gl_FragCoord.y / height - 0.5);

   //下面代码控制右侧图片
    //  0 to 2pi
   //theta = 2.0 * 3.14159265 * (gl_FragCoord.x / width);
    // -pi/2 to pi/2
   //phi = 3.14159265 * (gl_FragCoord.y / height - 0.5);

   // Vector in 3D space
   psph.x = cos(phi) * sin(theta);
   psph.y = cos(phi) * cos(theta);
   psph.z = sin(phi);

   // Calculate fisheye angle and radius
   theta = atan(psph.z,psph.x);
   phi = atan(sqrt(psph.x*psph.x+psph.z*psph.z),psph.y);
   r = width * phi / FOV;

   // Pixel in fisheye space
   pfish.x = (0.5 * width + r * cos(theta));
   pfish.y = (0.5 * width + r * sin(theta));

   float xoff = pfish.x/width;
   float yoff = pfish.y/width;

//原图
    //gl_FragColor = texture2D(sTexture, gl_TexCoord[0].st);
	gl_FragColor = texture2D(sTexture, vec2(xoff, yoff));
}