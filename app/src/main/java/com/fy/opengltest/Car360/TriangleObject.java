package com.fy.opengltest.Car360;

import android.content.Context;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.fy.opengltest.MatrixUtils.Camera;
import com.fy.opengltest.MatrixUtils.Model;
import com.fy.opengltest.MatrixUtils.Projection;
import com.fy.opengltest.R;
import com.fy.opengltest.ShaderUtils;
import com.fy.opengltest.renderPicture.TextureHelper;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by android on 8/18/17.
 */

public class TriangleObject extends IObject{
    private Context context;
    private Uri uri;
    private int programId;
    private int aPositionHandle;
    private int uMatrixHandle;

    private int textureId;
    private int uTextureSamplerHandle;
    private int aTextureCoordHandle;

    private IPosGl mPos;

    private Projection projection = new Projection(Projection.TYPE_frustumM);

    private float[] mMVMatrix = new float[16];
    private float[] mMVPMatrix = new float[16];

    public TriangleObject(Context context, Uri uri, IPosGl posGl) {
        this.context = context;
        this.uri = uri;
        this.mPos = posGl;
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        String vertexShader = ShaderUtils.readRawTextFile(context, R.raw.vertex_shader_render_picture);
        String fragmentShader = ShaderUtils.readRawTextFile(context, R.raw.fragment_shader_render_picture);
        programId = ShaderUtils.createProgram(vertexShader, fragmentShader);
        aPositionHandle = GLES20.glGetAttribLocation(programId, "aPosition");
        uMatrixHandle = GLES20.glGetUniformLocation(programId, "uMatrix");

        if (null != uri)
            textureId = TextureHelper.loadTexture(context, uri);
        else
            textureId = TextureHelper.loadTexture(context, R.raw.dome190_24402400);//R.raw.demo_pic);

        uTextureSamplerHandle = GLES20.glGetUniformLocation(programId, "sTexture");
        aTextureCoordHandle = GLES20.glGetAttribLocation(programId, "aTexCoord");

    }

    public void onSurfaceChanged(GL10 gl, int width, int height) {
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.test, options);
//        updateProjection(bitmap.getWidth(), bitmap.getHeight(), width, height);
//        bitmap.recycle();

        //updateProjection(9, 16, width, height);
        updateProjection(1, 1, width, height);
    }

    private void applyPos() {
        // This multiplies the view matrix by the mPos.getModel() matrix, and stores the result in the MVP matrix
        // (which currently contains mPos.getModel() * view).
        Matrix.multiplyMM(mMVMatrix, 0, mPos.getCamera().getMatrix(), 0, mPos.getModel().getMatrix(), 0);

        // This multiplies the mPos.getModel() view matrix by the projection matrix, and stores the result in the MVP matrix
        // (which now contains mPos.getModel() * view * projection).
        Matrix.multiplyMM(mMVPMatrix, 0, projection.getMartix(), 0, mMVMatrix, 0);
    }

    public void onDrawFrame(GL10 gl) {
        GLES20.glUseProgram(programId);
        applyPos();
        GLES20.glUniformMatrix4fv(uMatrixHandle, 1, false, mMVPMatrix, 0);
        GLES20.glEnableVertexAttribArray(aPositionHandle);
        GLES20.glVertexAttribPointer(aPositionHandle, 3, GLES20.GL_FLOAT, false, 0, mPos.getVertexBuffer());

        GLES20.glEnableVertexAttribArray(aTextureCoordHandle);
        GLES20.glVertexAttribPointer(aTextureCoordHandle, 2, GLES20.GL_FLOAT, false, 0, mPos.getTexVertexBuffer());

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);

        GLES20.glUniform1i(uTextureSamplerHandle, 0);

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, mPos.getIndexLength(), GLES20.GL_UNSIGNED_SHORT, mPos.getIndexBuffer());
    }

    private void updateProjection(int videoWidth, int videoHeight, int screenWidth, int screenHeight) {
        float screenRatio = (float) screenWidth / screenHeight;
//        float videoRatio = (float) videoWidth / videoHeight;
//        if (videoRatio > screenRatio) {
//            projection.setLeft(-1f);
//            projection.setRight(1f);
//            projection.setBottom(-videoRatio / screenRatio);
//            projection.setTop(videoRatio / screenRatio);
//            projection.setNear(0.3f);
//            projection.setFar(100f);
//        } else {
            projection.setLeft(-1f);
            projection.setRight(1f);
            projection.setBottom(-screenRatio);
            projection.setTop(screenRatio);
            projection.setNear(0.3f);
            projection.setFar(100f);
//        }
    }

    public void handValueChange(int type, int index, float change) {
        switch (type) {
            case 0:
                handCamera(index, change);
                break;
            case 1:
                handMode(index, change);
                break;
        }
    }

    private void handMode(int param, float change) {
        switch (param) {
            case 0:
                mPos.getModel().setX(change);
                break;
            case 1:
                mPos.getModel().setY(change);
                break;
            case 2:
                mPos.getModel().setZ(change);
                break;
            case 3:
                mPos.getModel().setAngleX(change);
                break;
            case 4:
                mPos.getModel().setAngleY(change);
                break;
            case 5:
                mPos.getModel().setAngleZ(change);
                break;
            case 6:
                mPos.getModel().setPitch(change);
                break;
            case 7:
                mPos.getModel().setYaw(change);
                break;
            case 8:
                mPos.getModel().setRoll(change);
                break;
        }
    }

    private void handCamera(int param, float change) {
        switch (param) {
            case 0:
                mPos.getCamera().setEyeX(change);
                break;
            case 1:
                mPos.getCamera().setEyeY(change);
                break;
            case 2:
                mPos.getCamera().setEyeZ(change);
                break;
            case 3:
                mPos.getCamera().setLookX(change);
                break;
            case 4:
                mPos.getCamera().setLookY(change);
                break;
            case 5:
                mPos.getCamera().setLookZ(change);
                break;
            case 6:
                mPos.getCamera().setUpX(change);
                break;
            case 7:
                mPos.getCamera().setUpY(change);
                break;
            case 8:
                mPos.getCamera().setUpZ(change);
                break;
        }
    }

    public String getParams() {
        return mPos.getCamera().toString() + "\n" + mPos.getModel().toString();
    }

    public float getValue(int type, int index) {
        switch (type) {
            case 0:
                return getCameraParam(index);
            case 1:
                return getModelParam(index);
            default:
                return 0f;
        }
    }


    private float getModelParam(int param) {
        float ret = 0f;
        switch (param) {
            case 0:
                ret = mPos.getModel().getX();
                break;
            case 1:
                ret = mPos.getModel().getY();
                break;
            case 2:
                ret = mPos.getModel().getZ();
                break;
            case 3:
                ret = mPos.getModel().getAngleX();
                break;
            case 4:
                ret = mPos.getModel().getAngleY();
                break;
            case 5:
                ret = mPos.getModel().getAngleZ();
                break;
            case 6:
                ret = mPos.getModel().getPitch();
                break;
            case 7:
                ret = mPos.getModel().getYaw();
                break;
            case 8:
                ret = mPos.getModel().getRoll();
                break;
        }
        return ret;
    }

    private float getCameraParam(int param) {
        float ret = 0f;
        switch (param) {
            case 0:
                ret = mPos.getCamera().getEyeX();
                break;
            case 1:
                ret = mPos.getCamera().getEyeY();
                break;
            case 2:
                ret = mPos.getCamera().getEyeZ();
                break;
            case 3:
                ret = mPos.getCamera().getLookX();
                break;
            case 4:
                ret = mPos.getCamera().getLookY();
                break;
            case 5:
                ret = mPos.getCamera().getLookZ();
                break;
            case 6:
                ret = mPos.getCamera().getUpX();
                break;
            case 7:
                ret = mPos.getCamera().getUpY();
                break;
            case 8:
                ret = mPos.getCamera().getUpZ();
                break;
        }
        return ret;
    }
}
