package com.fy.opengltest.Car360;

import android.opengl.GLSurfaceView;

/**
 * Created by android on 8/19/17.
 */

public abstract class IObject implements GLSurfaceView.Renderer {
    abstract public String getParams();

    abstract public void handValueChange(int type, int index, float change);

    abstract public float getValue(int type, int index);
}
