package com.fy.opengltest.Car360;

import com.fy.opengltest.MatrixUtils.Camera;
import com.fy.opengltest.MatrixUtils.Model;

/**
 * Created by android on 8/19/17.
 */

public class BlockRightPosGl extends AbsBlockPosGl{
    private static final float[] vertexData = {
            1f, 1f, 0f,
            -1f, 1f, 0f,
            0f, 0f, 0f
    };

    private static final short[] indexData = {
            0, 1, 2,
    };

    public BlockRightPosGl() {
        super(vertexData, indexData);
    }

}
