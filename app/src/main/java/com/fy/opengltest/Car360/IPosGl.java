package com.fy.opengltest.Car360;

import com.fy.opengltest.MatrixUtils.Camera;
import com.fy.opengltest.MatrixUtils.Model;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by android on 8/19/17.
 */

public interface IPosGl {

    FloatBuffer getVertexBuffer();
    FloatBuffer getTexVertexBuffer();
    ShortBuffer getIndexBuffer();

    int getIndexLength();

    void rotation(int angle);
    void translateLeft(float dis);
    void translateRight(float dis);

    void bigger(float dis);
    void small(float dis);

    Camera getCamera();

    Model getModel();
}
