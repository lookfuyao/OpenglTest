package com.fy.opengltest.Car360;

import android.content.Context;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.fy.opengltest.MatrixUtils.Projection;
import com.fy.opengltest.R;
import com.fy.opengltest.ShaderUtils;
import com.fy.opengltest.renderPicture.TextureHelper;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by android on 8/18/17.
 */

public class SphereTriangleObject extends TriangleObject{

    SpherePosGl spherePosGl;

    public SphereTriangleObject(Context context, Uri uri, SpherePosGl posGl) {
        super(context, uri, posGl);
        spherePosGl = posGl;
    }

    public void handValueChange(int type, int index, float change) {
        switch (type) {
            case 2:
                spherePosGl.handCalibrate(index, change);
                break;
            default:
                super.handValueChange(type, index, change);
        }
    }

    public String getParams() {
        return spherePosGl.getParams();
    }

    public float getValue(int type, int index) {
        switch (type) {
            case 2:
                return spherePosGl.getValue(index);
            default:
                return super.getValue(type, index);
        }
    }

}
