package com.fy.opengltest.Car360;

import android.graphics.PointF;

/**
 * Created by android on 8/19/17.
 */

public class PostionHelper {

    private final PointF texPos0 = new PointF();
    private final PointF textPos1 = new PointF();
    private final PointF textPos2 = new PointF();
    private final PointF textPos3 = new PointF();

    private final float[] vertexDataFront = {
            0f, 0f, 0f,
            1f, 1f, 0f,
            0f, 1f, 0f,
            -1f, -1f, 0f,
    };

    private final short[] indexDataFront = {
            0, 1, 2,
            0, 2, 3
    };

    private final float[] textureVertexData = {
            1f, 0f,
            0f, 0f,
            0f, 1f,
            1f, 1f
    };


}
