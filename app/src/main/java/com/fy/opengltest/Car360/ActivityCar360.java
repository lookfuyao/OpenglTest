package com.fy.opengltest.Car360;

import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.fy.opengltest.R;
import com.fy.opengltest.SpinnerHelper;
import com.fy.opengltest.renderPicture.GLRendererMultiPicture;

import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ActivityCar360 extends AppCompatActivity {


    private GLSurfaceView glSurfaceView;
    private GLRendererMultiPicture mRenderer = null;

    private TextView paramsTextView;

    MDTouchHelper mdTouchHelper;
    SpinnerHelper mSpinnerHelper;
    int mSelectItem;
    int mSelectType = 1;
    int mSelectParam;

    private MDPinchConfig pinchConfig = new MDPinchConfig();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car360);
        paramsTextView = (TextView) findViewById(R.id.params);
        glSurfaceView = (GLSurfaceView) findViewById(R.id.gl_surface_view);
        glSurfaceView.setEGLContextClientVersion(2);
        List<Uri> uris = new ArrayList<>();
        uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car1080/left.png")));
        uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car1080/right.png")));
        uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car1080/back.png")));
        uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car1080/front.png")));
        mRenderer = new GLRendererMultiPicture(this, uris);
        glSurfaceView.setRenderer(mRenderer);
        mdTouchHelper = new MDTouchHelper(this);
        mdTouchHelper.setGestureListener(gestureListener);
        mdTouchHelper.setFlingConfig(new MDFlingConfig());
        mdTouchHelper.setPinchConfig(pinchConfig);
        glSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mdTouchHelper.handleTouchEvent(event);
            }
        });

        SparseArray<String> data = new SparseArray<>();
        data.put(0, "Left");
        data.put(1, "Right");
        data.put(2, "Back");
        data.put(3, "Front");
        SpinnerHelper spinnerHelper = SpinnerHelper.with(this)
                .setData(data)
                .setDefault(mSelectItem)
                .setClickHandler(new SpinnerHelper.ClickHandler() {
                    @Override
                    public void onSpinnerClicked(int index, int key, String value) {
                        if (mSelectItem != key) {
                            mSelectItem = key;
                            updateRange(mSelectItem, mSelectType, mSelectParam);
                        }
                    }
                });
        spinnerHelper.init(R.id.control_item);

        data = new SparseArray<>();
        data.put(0, "Camera");
        data.put(1, "Model");

        spinnerHelper = SpinnerHelper.with(this)
                .setDefault(mSelectType)
                .setData(data)
                .setClickHandler(new SpinnerHelper.ClickHandler() {
                    @Override
                    public void onSpinnerClicked(int index, int key, String value) {
                        if (mSelectType == key) {
                            return;
                        }
                        onTypeChange(key);
                        updateRange(mSelectItem, mSelectType, mSelectParam);
                    }
                });
        spinnerHelper.init(R.id.control_type);

        mSpinnerHelper = SpinnerHelper.with(this)
                .setData(mSelectType == 1 ? model : camera)
                .setDefault(mSelectParam)
                .setClickHandler(new SpinnerHelper.ClickHandler() {
                    @Override
                    public void onSpinnerClicked(int index, int key, String value) {
                        if (mSelectParam == key) {
                            return;
                        }
                        mSelectParam = key;
                        updateRange(mSelectItem, mSelectType, mSelectParam);
                    }
                });
        mSpinnerHelper.init(R.id.control_param);
        updateText();
        updateRange(mSelectItem, mSelectType, mSelectParam);
    }

    void updateRange(int item, int type, int param) {
        float value = mRenderer.getValue(item, type, param);
        float max = 0f;
        float min = 0f;
        switch (type) {
            case 0:
                min = rangeCamera[param][0];
                max = rangeCamera[param][1];
                break;
            case 1:
                min = rangeModel[param][0];
                max = rangeModel[param][1];
                break;
        }
        pinchConfig.setDefaultValue(value);
        pinchConfig.setMax(max);
        pinchConfig.setMin(min);
        pinchConfig.setSensitivity((max - min) / 2f);
        mdTouchHelper.setPinchConfig(pinchConfig);
        Log.d(TAG, " value=" + value + " max=" + max + " min=" + min + " sens=" + ((max - min) / 2f));
    }

    void updateText() {
        paramsTextView.setText(mRenderer.getParams(mSelectItem));
    }

    void onTypeChange(int key) {
        mSelectType = key;
        switch (key) {
            case 0:
                mSpinnerHelper.updateData(camera, R.id.control_param);
                break;
            case 1:
                mSpinnerHelper.updateData(model, R.id.control_param);
                break;
        }
    }

    static SparseArray<String> camera = new SparseArray<>();
    static SparseArray<String> model = new SparseArray<>();

    static float[][] rangeCamera = {
            {-1000f, 1000f},
            {-1000f, 1000f},
            {-1000f, 1000f},

            {-1f, 1f},
            {-1f, 1f},
            {-1f, 1f},

            {-1f, 1f},
            {-1f, 1f},
            {-1f, 1f},
    };

    static float[][] rangeModel = {
            {-1f, 1f},
            {-1f, 1f},
            {-1f, 6f},

            {0f, 360f},
            {0f, 360f},
            {0f, 360f},

            {0f, 360f},
            {0f, 360f},
            {0f, 360f},
    };

    static {
        camera.put(0, "eyeX");
        camera.put(1, "eyeY");
        camera.put(2, "eyeZ");
        camera.put(3, "lookX");
        camera.put(4, "lookY");
        camera.put(5, "lookZ");
        camera.put(6, "upX");
        camera.put(7, "upY");
        camera.put(8, "upZ");

        model.put(0, "mX");
        model.put(1, "mY");
        model.put(2, "mZ");
        model.put(3, "mAngleX");
        model.put(4, "mAngleY");
        model.put(5, "mAngleZ");
        model.put(6, "mPitch(x-axis)");
        model.put(7, "mYaw(y-axis)");
        model.put(8, "mRoll(z-axis)");
    }

    private static final String TAG = "Car360";
    MDTouchHelper.GestureListener gestureListener = new MDTouchHelper.GestureListener() {

        @Override
        public boolean onDrag(float dis_x, float dis_y) {
//            Log.d(TAG, "onDrag dis_x=" + dis_x + " dis_y=" + dis_y);
            return false;
        }

        @Override
        public boolean onClick(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onPinch(float dis) {
            Log.d(TAG, "onPinch dis=" + dis);
            mRenderer.onChange(mSelectItem, mSelectType, mSelectParam, dis);
            updateText();
            return false;
        }
    };
}
