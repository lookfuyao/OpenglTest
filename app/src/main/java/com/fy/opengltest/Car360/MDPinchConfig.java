package com.fy.opengltest.Car360;

/**
 * Created by hzqiujiadi on 16/9/29.
 * hzqiujiadi ashqalcn@gmail.com
 */

public class MDPinchConfig {
    private float max = 100;
    private float min = -100;
    private float defaultValue = 0;
    private float mSensitivity = 30;

    public MDPinchConfig setMax(float max) {
        this.max = max;
        return this;
    }

    public MDPinchConfig setMin(float min) {
        this.min = min;
        return this;
    }

    public MDPinchConfig setDefaultValue(float defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    public MDPinchConfig setSensitivity(float mSensitivity) {
        this.mSensitivity = mSensitivity;
        return this;
    }

    public float getSensitivity() {
        return mSensitivity;
    }

    public float getMax() {
        return max;
    }

    public float getMin() {
        return min;
    }

    public float getDefaultValue() {
        return defaultValue;
    }
}
