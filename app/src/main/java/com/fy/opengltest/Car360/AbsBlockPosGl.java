package com.fy.opengltest.Car360;

import com.fy.opengltest.MatrixUtils.Camera;
import com.fy.opengltest.MatrixUtils.Model;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by android on 8/19/17.
 */

public abstract class AbsBlockPosGl implements IPosGl {

    private float textVer0x = 0.5f;
    private float textVer0y = 1f;
    private float textVer1x = 0.8f;
    private float textVer1y = 0.3f;
    private float textVer2x = 0.2f;
    private float textVer2y = 0.3f;
    boolean change = true;


    protected Camera camera = new Camera();
    protected Model model = new Model(0, 0, 2f, 0, 0, 0, 0, 0, 180);

    private float[] textureVertexData = new float[6];

    private FloatBuffer vertexBuffer;
    private ShortBuffer indexBuffer;
    private FloatBuffer textureVertexBuffer;

    private float[] vertexData;

    private short[] indexData;
    private static final String TAG = "AbsBlockPosGl";

    public AbsBlockPosGl(float[] vertexData, short[] indexData) {
        this.vertexData = vertexData;
        this.indexData = indexData;
    }

    @Override
    public FloatBuffer getVertexBuffer() {
        if (vertexBuffer == null) {
            vertexBuffer = ByteBuffer.allocateDirect(vertexData.length * 4)
                    .order(ByteOrder.nativeOrder())
                    .asFloatBuffer()
                    .put(vertexData);
            vertexBuffer.position(0);
        }
//        Log.d(TAG, "getVertexBuffer " + vertexBuffer.toString());
        return vertexBuffer;
    }

    @Override
    public ShortBuffer getIndexBuffer() {
        if (null == indexBuffer) {
            indexBuffer = ByteBuffer.allocateDirect(indexData.length * 2)
                    .order(ByteOrder.nativeOrder())
                    .asShortBuffer()
                    .put(indexData);
            indexBuffer.position(0);
        }
//        Log.d(TAG, "getIndexBuffer " + indexBuffer.toString());
        return indexBuffer;
    }

    @Override
    public int getIndexLength() {
        return indexData.length;
    }

    @Override
    public FloatBuffer getTexVertexBuffer() {
        if (null == textureVertexBuffer || change) {
            textureVertexData[0] = textVer0x;
            textureVertexData[1] = textVer0y;
            textureVertexData[2] = textVer1x;
            textureVertexData[3] = textVer1y;
            textureVertexData[4] = textVer2x;
            textureVertexData[5] = textVer2y;
            change = false;
            textureVertexBuffer = ByteBuffer.allocateDirect(textureVertexData.length * 4)
                    .order(ByteOrder.nativeOrder())
                    .asFloatBuffer()
                    .put(textureVertexData);
            textureVertexBuffer.position(0);
        }
//        Log.d(TAG, "getTexVertexBuffer " + textureVertexBuffer.toString());
        return textureVertexBuffer;
    }


    @Override
    public void rotation(int angle) {

    }

    @Override
    public void translateLeft(float dis) {

    }

    @Override
    public void translateRight(float dis) {

    }

    @Override
    public void bigger(float dis) {

    }

    @Override
    public void small(float dis) {

    }

    @Override
    public Camera getCamera() {
        return camera;
    }

    @Override
    public Model getModel() {
        return model;
    }
}
