//package com.fy.opengltest.renderPolygn;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.net.Uri;
//import android.opengl.GLES20;
//import android.opengl.GLSurfaceView;
//import android.opengl.Matrix;
//
//import com.fy.opengltest.R;
//import com.fy.opengltest.ShaderUtils;
//import com.fy.opengltest.renderPicture.TextureHelper;
//
//import java.nio.ByteBuffer;
//import java.nio.ByteOrder;
//import java.nio.FloatBuffer;
//import java.nio.ShortBuffer;
//
//import javax.microedition.khronos.egl.EGLConfig;
//import javax.microedition.khronos.opengles.GL10;
//
///**
// * Created by yao.fu on 17-1-23.
// */
//public class GLRendererFishCilibrationJpan implements GLSurfaceView.Renderer {
//
//    private Uri uri;
//    private Context context;
//    private int programId;
//
//    private int textureId;
//    private float aspect = 1f;
//
//    private int screen_samples =1271;                    
//
//
//    private float rotation = 100f;
//
//    private int  gapLoc;//(glGetUniformLocation(expansion, "gap"));
//    private int  screenLoc;//(glGetUniformLocation(expansion, "screen"));
//    private int  focalLoc;//(glGetUniformLocation(expansion, "focal"));
//    private int  rotationLoc;//(glGetUniformLocation(expansion, "rotation"));
//    private int  circleLoc;//(glGetUniformLocation(expansion, "circle"));
//    private int  imageLoc;//(glGetUniformLocation(expansion, "image"));                         
//
//
//    public GLRendererFishCilibrationJpan(Context context) {
//        this.context = context;
//    }
//
//    public GLRendererFishCilibrationJpan(Context context, Uri uri) {
//        this.context = context;
//        this.uri = uri;
//    }
//
//    @Override
//    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
//        String vertexShader = ShaderUtils.readRawTextFile(context, R.raw.fisheye);
//        String fragmentShader = ShaderUtils.readRawTextFile(context, R.raw.normal);
//        programId = ShaderUtils.createProgram(vertexShader, fragmentShader);
//
//        gapLoc = GLES20.glGetUniformLocation(programId, "gap");
//        screenLoc = GLES20.glGetUniformLocation(programId, "screen");
//        focalLoc = GLES20.glGetUniformLocation(programId, "uMatrix");
//        rotationLoc = GLES20.glGetUniformLocation(programId, "rotation");
//        circleLoc = GLES20.glGetUniformLocation(programId, "circle");
//        imageLoc = GLES20.glGetUniformLocation(programId, "image");
//
//        if (null != uri)
//            textureId = TextureHelper.loadTexture(context, uri);
//        else
//            textureId = TextureHelper.loadTexture(context, R.raw.dome190_24402400);//R.raw.demo_pic);
//    }
//
//    @Override
//    public void onSurfaceChanged(GL10 gl, int width, int height) {
////        if (null != uri) {
////            Bitmap bmp = TextureHelper.getBitmap(context, uri);
////            if (null != bmp) {
////                updateProjection(bmp.getWidth(), bmp.getHeight(), width, height);
////            }
////        } else {
////            updateProjection(1, 1, width, height);
////        }
//        aspect = ((float)width)/((float) height);
//    }
//
//    @Override
//    public void onDrawFrame(GL10 gl) {
//
//        //GLES20.glViewport(0, 0, screenWidth, screenHeight);
//
//        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
//        GLES20.glUseProgram(programId);
//
//        int slices = (int) Math.sqrt(aspect * screen_samples);
//        int stacks = (screen_samples / slices - 1); // 描画するインスタンスの数なので先に 1 を引いておく。
//
//
//        GLES20.glUniform2f(gapLoc, 2.0f / (slices - 1), 2.0f / stacks);                    
//
//        float screen[] = { aspect, 1.0f, 0.0f, 0.0f };
//        GLES20.glUniform4fv(screenLoc, 1, screen, 0);                     
//
//        GLES20.glUniform1f(focalLoc, -50.0f / (rotation - 50.0f));                    
//
//
//        GLES20.glUniformMatrix4fv(rotationLoc, 1, true, window.getLeftTrackball().get());                    
//
//        float circle[] =
//                {
//                        capture_circle[0] + window.getShiftWheel() * 0.001f,
//                        capture_circle[1] + window.getShiftWheel() * 0.001f,
//                        capture_circle[2] + (window.getShiftArrowX() - window.getControlArrowX()) * 0.001f,
//                        capture_circle[3] + (window.getShiftArrowY() + window.getControlArrowY()) * 0.001f
//                };
//        GLES20.glUniform4fv(circleLoc, 1, circle);                           
//
//        GLES20.glUniform1i(imageLoc, 0);                    
//
//        GLES20.glUniformMatrix4fv(uMatrixHandle, 1, false, projectionMatrix, 0);
//
//        GLES20.glEnableVertexAttribArray(aVertexPosition);
//        GLES20.glVertexAttribPointer(aVertexPosition, 3, GLES20.GL_FLOAT, false, 12, vertexBuffer);
//
//        //GLES20.glEnableVertexAttribArray(aTextureCoord);
//        //GLES20.glVertexAttribPointer(aTextureCoord, 2, GLES20.GL_FLOAT, false, 8, textureVertexBuffer);
//
//        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
//        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId);
//        GLES20.glUniform1i(uSampler, 0);
//
//        mParam_lens[0] = mParam_a;
//        mParam_lens[1] = mParam_b;
//        mParam_lens[2] = mParam_f;
//        mParam_lens[3] = mParam_scale;
//
//        GLES20.glUniform4fv(uLens, 1, mParam_lens, 0);
//
//        mParam_fovs[0] = mParam_x;
//        mParam_fovs[1] = mParam_y;
//        GLES20.glUniform2fv(uFov, 1, mParam_fovs, 0);
//
//        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indexData.length, GLES20.GL_UNSIGNED_SHORT, indexBuffer);
//
//    }
//
//}
