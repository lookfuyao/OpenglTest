package com.fy.opengltest;

import android.app.Application;
import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by android on 8/19/17.
 */

public class OpenglApplication extends Application {
    private static final String TAG = "OpenglApplication";

    public static final String TEST_PIC_PATH = Environment.getExternalStorageDirectory() + "/vr";


    @Override
    public void onCreate() {
        super.onCreate();
        initTestFile();
    }

    public void initTestFile() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                copyFilesFassets(getApplicationContext(), "", TEST_PIC_PATH);
            }
        };
        thread.start();
    }

    public void copyFilesFassets(Context context, String oldPath, String newPath) {
        try {
            String fileNames[] = context.getAssets().list(oldPath);//获取assets目录下的所有文件及目录名
            if (fileNames.length > 0) {//如果是目录
                File file = new File(newPath);
                file.mkdirs();//如果文件夹不存在，则递归
                for (String fileName : fileNames) {
                    String path = "";
                    if (TextUtils.isEmpty(oldPath)){
                        path = fileName;
                    } else {
                        path = oldPath + "/" + fileName;
                    }
                    copyFilesFassets(context, path, newPath + "/" + fileName);
                }
            } else {//如果是文件
                Log.e(TAG, "copyFilesFassets oldPath:" + oldPath);
                InputStream is = context.getAssets().open(oldPath);
                FileOutputStream fos = new FileOutputStream(new File(newPath));
                byte[] buffer = new byte[1024];
                int byteCount = 0;
                while ((byteCount = is.read(buffer)) != -1) {//循环从输入流读取 buffer字节
                    fos.write(buffer, 0, byteCount);//将读取的输入流写入到输出流
                }
                fos.flush();//刷新缓冲区
                is.close();
                fos.close();
            }
        } catch (Exception e) {
            Log.e(TAG, "copyFilesFassets Exception:", e);
        }
    }

}
