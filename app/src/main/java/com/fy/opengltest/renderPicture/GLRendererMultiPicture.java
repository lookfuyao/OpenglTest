package com.fy.opengltest.renderPicture;

import android.content.Context;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import com.fy.opengltest.Car360.AbsBlockPosGl;
import com.fy.opengltest.Car360.BlockBackPosGl;
import com.fy.opengltest.Car360.BlockFrontPosGl;
import com.fy.opengltest.Car360.BlockLeftPosGl;
import com.fy.opengltest.Car360.BlockRightPosGl;
import com.fy.opengltest.Car360.IObject;
import com.fy.opengltest.Car360.TriangleObject;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by android on 8/18/17.
 */

public class GLRendererMultiPicture implements GLSurfaceView.Renderer {

    private List<IObject> mObjects = new ArrayList<>();

    public String getParams(int indentify) {
        return mObjects.get(indentify).getParams();
    }

    public GLRendererMultiPicture(Context context, List<Uri> uriList) {
        Uri uri = uriList.get(0);
        AbsBlockPosGl posGl = new BlockLeftPosGl();
        IObject object = new TriangleObject(context, uri, posGl);
        mObjects.add(object);

        uri = uriList.get(1);
        posGl = new BlockRightPosGl();
        object = new TriangleObject(context, uri, posGl);
        mObjects.add(object);

        uri = uriList.get(2);
        posGl = new BlockBackPosGl();
        object = new TriangleObject(context, uri, posGl);
        mObjects.add(object);

        uri = uriList.get(3);
        posGl = new BlockFrontPosGl();
        object = new TriangleObject(context, uri, posGl);
        mObjects.add(object);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        for (IObject object : mObjects) {
            object.onSurfaceCreated(gl, config);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        for (IObject object : mObjects) {
            object.onSurfaceChanged(gl, width, height);
        }
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        for (IObject object : mObjects) {
            object.onDrawFrame(gl);
        }
        GLES20.glDisable(GLES20.GL_BLEND);
    }

    public void onChange(int indentify, int type, int index, float value) {
        mObjects.get(indentify).handValueChange(type, index, value);
    }

    public float getValue(int indentify, int type, int index) {
        return mObjects.get(indentify).getValue(type, index);
    }
}
