package com.fy.opengltest.renderPicture;

import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.fy.opengltest.Car360.AbsBlockPosGl;
import com.fy.opengltest.Car360.BlockBackPosGl;
import com.fy.opengltest.Car360.BlockFrontPosGl;
import com.fy.opengltest.Car360.BlockLeftPosGl;
import com.fy.opengltest.Car360.BlockRightPosGl;
import com.fy.opengltest.Car360.TriangleObject;
import com.fy.opengltest.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class ActivityPictureObject extends AppCompatActivity implements GLSurfaceView.Renderer {

    private GLSurfaceView glSurfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_object);

        glSurfaceView = (GLSurfaceView) findViewById(R.id.gl_surface_view);
        glSurfaceView.setEGLContextClientVersion(2);

        List<Uri> uris = new ArrayList<>();
        uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car1080/left.png")));
        uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car1080/right.png")));
        uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car1080/back.png")));
        uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car1080/front.png")));
        initRenderer(uris);
        glSurfaceView.setRenderer(this);
    }

    private List<PictureObject> mObjects = new ArrayList<>();

    private void initRenderer(List<Uri> uriList) {
        for (Uri uri : uriList) {
            PictureObject object = new PictureObject(this, uri);
            mObjects.add(object);
        }
    }


    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        for (PictureObject object : mObjects) {
            object.onSurfaceCreated(gl, config);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        for (PictureObject object : mObjects) {
            object.onSurfaceChanged(gl, width, height);
        }
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
        for (PictureObject object : mObjects) {
            object.onDrawFrame(gl);
        }
    }
}
