package com.fy.opengltest.renderPicture;

import android.content.Context;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import com.fy.opengltest.Car360.AbsBlockPosGl;
import com.fy.opengltest.Car360.BlockBackPosGl;
import com.fy.opengltest.Car360.BlockFrontPosGl;
import com.fy.opengltest.Car360.BlockLeftPosGl;
import com.fy.opengltest.Car360.SpherePosGl;
import com.fy.opengltest.Car360.IObject;
import com.fy.opengltest.Car360.SpherePosGl;
import com.fy.opengltest.Car360.SphereTriangleObject;
import com.fy.opengltest.Car360.TriangleObject;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by android on 8/18/17.
 */

public class GLRendererMultiPictureSphere implements GLSurfaceView.Renderer {

    private List<IObject> mObjects = new ArrayList<>();

    public String getParams(int indentify) {
        return mObjects.get(indentify).getParams();
    }

    public GLRendererMultiPictureSphere(Context context, List<Uri> uriList) {
        Uri uri = uriList.get(0);
        SpherePosGl posGl = new SpherePosGl();
        IObject object = new SphereTriangleObject(context, uri, posGl);

        mObjects.add(object);

        uri = uriList.get(1);
        posGl = new SpherePosGl();
        object = new SphereTriangleObject(context, uri, posGl);
        mObjects.add(object);

        uri = uriList.get(2);
        posGl = new SpherePosGl();
        object = new SphereTriangleObject(context, uri, posGl);
        mObjects.add(object);

        uri = uriList.get(3);
        posGl = new SpherePosGl();
        object = new SphereTriangleObject(context, uri, posGl);
        object.handValueChange(0, 0, 0);
        object.handValueChange(0, 1, 0);
        object.handValueChange(0, 2, 0);

        object.handValueChange(0, 3, 0);
        object.handValueChange(0, 4, 0);
        object.handValueChange(0, 5, 1);

        object.handValueChange(0, 6, 0);
        object.handValueChange(0, 7, 1);
        object.handValueChange(0, 8, 0);


        object.handValueChange(1, 0, 0);
        object.handValueChange(1, 1, 0);
        object.handValueChange(1, 2, 0);

        object.handValueChange(1, 3, 0);
        object.handValueChange(1, 4, 0);
        object.handValueChange(1, 5, 100);

        object.handValueChange(1, 6, 77);
        object.handValueChange(1, 7, 100);
        object.handValueChange(1, 8, 60);
        mObjects.add(object);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        for (IObject object : mObjects) {
            object.onSurfaceCreated(gl, config);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        for (IObject object : mObjects) {
            object.onSurfaceChanged(gl, width, height);
        }
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        for (IObject object : mObjects) {
            object.onDrawFrame(gl);
        }
        GLES20.glDisable(GLES20.GL_BLEND);
    }

    public void onChange(int indentify, int type, int index, float value) {
        mObjects.get(indentify).handValueChange(type, index, value);
    }

    public float getValue(int indentify, int type, int index) {
        return mObjects.get(indentify).getValue(type, index);
    }
}
