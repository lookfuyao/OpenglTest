package com.fy.opengltest;

/**
 * Created by android on 8/18/17.
 */

public interface IChange {
    void onChange(int indentify, int index, float value);
    String getTitle(int indentify, int index);
    float getValue(int indentify, int index);
}
