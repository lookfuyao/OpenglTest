package com.fy.opengltest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.fy.opengltest.FourBlockCalibrate.GLRendererBlocksCilibration;
import com.fy.opengltest.renderPicture.GLRendererCirclePicture;
import com.fy.opengltest.renderPicture.GLRendererPicture;
import com.fy.opengltest.renderPolygn.GLRendererCircle;
import com.fy.opengltest.renderPolygn.GLRendererCircle2Rectangle;
import com.fy.opengltest.renderPolygn.GLRendererFishCilibration;
import com.fy.opengltest.renderPolygn.GLRendererFishToNormal;
import com.fy.opengltest.renderPolygn.GLRendererQuatrangle;
import com.fy.opengltest.renderPolygn.GLRendererTriangle;
import com.fy.opengltest.renderSphere.GLSphereObjectRenderer;
import com.fy.seekbarex.SeekBarFloat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private GLSurfaceView glSurfaceView;

    private AbsRenderer mRenderer;

    private int mIndentify = 0;

    public static final int TYPE_TRIANGLE = 0;
    public static final int TYPE_Quatrangle = 1;
    public static final int TYPE_PICTURE = 2;
    public static final int TYPE_CIRCLE_PICTURE = 3;
    public static final int TYPE_CIRCLE = 4;
    public static final int TYPE_FISH_TO_QUANJING = 5;
    public static final int TYPE_FishCilibration = 6;
    public static final int TYPE_FISH_4_BLOCK_Calibrate = 7;
    public static final int TYPE_Sphere = 8;
    public static final int TYPE_multi_Sphere = 9;
    public static final int TYPE_CIRCLE_2_RECTANGLE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int type = getType();
        if (type == -1) {
            finish();
            return;
        }

        glSurfaceView = (GLSurfaceView) findViewById(R.id.surface_view);
        glSurfaceView.setEGLContextClientVersion(2);
        initRender(type);
    }


    private void initRender(int type) {
        hideControl();
        switch (type) {
            case TYPE_TRIANGLE:
                glSurfaceView.setRenderer(new GLRendererTriangle(getApplicationContext()));
                break;

            case TYPE_Quatrangle:
                glSurfaceView.setRenderer(new GLRendererQuatrangle(getApplicationContext()));
                break;

            case TYPE_PICTURE:
                glSurfaceView.setRenderer(new GLRendererPicture(getApplicationContext(), getUri()));
                break;

            case TYPE_CIRCLE_PICTURE:
                glSurfaceView.setRenderer(new GLRendererCirclePicture(getApplicationContext(), getUri()));
                break;

            case TYPE_CIRCLE:
                glSurfaceView.setRenderer(new GLRendererCircle(getApplicationContext()));
                break;

            case TYPE_CIRCLE_2_RECTANGLE:
                glSurfaceView.setRenderer(new GLRendererCircle2Rectangle(getApplicationContext(), getUri()));
                break;

            case TYPE_Sphere:
                glSurfaceView.setRenderer(new GLSphereObjectRenderer(getApplicationContext(),  getUri()));
                break;

            case TYPE_FISH_TO_QUANJING:
                glSurfaceView.setRenderer(new GLRendererFishToNormal(getApplicationContext(), getUri()));
                break;

            case TYPE_FishCilibration:
                mRenderer = new GLRendererFishCilibration(getApplicationContext(), getUri());
                glSurfaceView.setRenderer(mRenderer);
                hasControl();
                break;
            case TYPE_FISH_4_BLOCK_Calibrate:
                List<Uri> uris = new ArrayList<>();
//                uris.add();
//                uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/one_1.jpg")));
//                uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/one_2.jpg")));
//                uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/one_3.jpg")));
//                uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/one_4.jpg")));
                uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car360/1.png")));
                uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car360/2.png")));
                uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car360/3.png")));
                uris.add(Uri.fromFile(new File("/storage/emulated/0/vr/car360/4.png")));
                GLRendererBlocksCilibration renderer = new GLRendererBlocksCilibration(getApplicationContext(), uris);
                glSurfaceView.setRenderer(renderer);
                break;
        }

//        glSurfaceView.setRenderer(new GLRendererSphere(getApplicationContext()));

//        glSurfaceView.setRenderer(new FBORendererCube(getApplicationContext()));

        glSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

    }

    private void hasControl(){
        initControl();
        glSurfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = findViewById(R.id.control_layout);
                if (view.getVisibility() != View.VISIBLE) {
                    view.setVisibility(View.VISIBLE);
                } else {
                    view.setVisibility(View.GONE);
                }
            }
        });
    }


    //Same to MDPinchConfig
    private float max = 100;
    private float min = -100;

    private float coverChange(float p_max, float p_min, float change){
        return (max - min) / (change * ( p_max - p_min));
    }

    private void seekBarChange(int indentify, int id, float progress){
        mRenderer.onChange(indentify, id, progress);
        String title = mRenderer.getTitle(indentify, id);
        switch (id) {
            case R.id.seekBarFx:
                ((TextView) findViewById(R.id.text_fx)).setText(title + progress);
                break;

            case R.id.seekBarFy:
                ((TextView) findViewById(R.id.text_fy)).setText(title + progress);
                break;

            case R.id.seekBar1:
                ((TextView) findViewById(R.id.text_a)).setText(title + progress);
                break;

            case R.id.seekBar2:
                ((TextView) findViewById(R.id.text_b)).setText(title + progress);
                break;

            case R.id.seekBar3:
                ((TextView) findViewById(R.id.text_scale)).setText(title + progress);
                break;

            case R.id.seekBar4:
                ((TextView) findViewById(R.id.text_x)).setText(title + progress);
                break;

            case R.id.seekBar5:
                ((TextView) findViewById(R.id.text_y)).setText(title + progress);
                break;
        }
    }

    private SeekBarFloat.OnSeekBarChangeListener onSeekBarChangeListenerEx = new SeekBarFloat.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBarFloat seekBarFloat, float progress, boolean fromUser) {
            seekBarChange(0, seekBarFloat.getId(), progress);
        }

        @Override
        public void onStartTrackingTouch(SeekBarFloat seekBarFloat) {
        }

        @Override
        public void onStopTrackingTouch(SeekBarFloat seekBarFloat) {
        }
    };

//    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
//        @Override
//        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//            switch (seekBar.getId()) {
//                case R.id.seekBar:
//                    ((TextView) findViewById(R.id.text_f)).setText("F:" + mRenderer.setF(progress));
//                    break;
//                case R.id.seekBar1:
//                    ((TextView) findViewById(R.id.text_a)).setText("a:" + mRenderer.setA(progress));
//                    break;
//                case R.id.seekBar2:
//                    ((TextView) findViewById(R.id.text_b)).setText("b:" + mRenderer.setB(progress));
//                    break;
//                case R.id.seekBar3:
//                    ((TextView) findViewById(R.id.text_scale)).setText("scale:" + mRenderer.setScale(progress));
//                    break;
//                case R.id.seekBar4:
//                    ((TextView) findViewById(R.id.text_x)).setText("x:" + mRenderer.setX(progress));
//                    break;
//                case R.id.seekBar5:
//                    ((TextView) findViewById(R.id.text_y)).setText("y:" + mRenderer.setY(progress));
//                    break;
//            }
//        }
//
//        @Override
//        public void onStartTrackingTouch(SeekBar seekBar) {
//
//        }
//
//        @Override
//        public void onStopTrackingTouch(SeekBar seekBar) {
//
//        }
//    };

    private void hideControl() {
        View view = findViewById(R.id.control_layout);
        view.setVisibility(View.GONE);
    }

    private void initControl() {
        SeekBarFloat seekbar = (SeekBarFloat) findViewById(R.id.seekBarFx);
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListenerEx);
        seekbar.setProgressF(mRenderer.getValue(mIndentify, seekbar.getId()));

        seekbar = (SeekBarFloat) findViewById(R.id.seekBarFy);
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListenerEx);
        seekbar.setProgressF(mRenderer.getValue(mIndentify, seekbar.getId()));


        seekbar = (SeekBarFloat) findViewById(R.id.seekBar1);
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListenerEx);
        seekbar.setProgressF(mRenderer.getValue(mIndentify, seekbar.getId()));

        seekbar = (SeekBarFloat) findViewById(R.id.seekBar2);
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListenerEx);
        seekbar.setProgressF(mRenderer.getValue(mIndentify, seekbar.getId()));


        seekbar = (SeekBarFloat) findViewById(R.id.seekBar3);
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListenerEx);
        seekbar.setProgressF(mRenderer.getValue(mIndentify, seekbar.getId()));


        seekbar = (SeekBarFloat) findViewById(R.id.seekBar4);
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListenerEx);
        seekbar.setProgressF(mRenderer.getValue(mIndentify, seekbar.getId()));

        seekbar = (SeekBarFloat) findViewById(R.id.seekBar5);
        seekbar.setOnSeekBarChangeListener(onSeekBarChangeListenerEx);
        seekbar.setProgressF(mRenderer.getValue(mIndentify, seekbar.getId()));
    }

    public static void start(Context context, Uri uri, Class<? extends Activity> clz, int rendertype) {
        Intent i = new Intent(context, clz);
        i.setData(uri);
        i.putExtra("type", rendertype);
        context.startActivity(i);
    }

    protected Uri getUri() {
        Intent i = getIntent();
        if (i == null || i.getData() == null) {
            return null;
        }
        return i.getData();
    }

    private int getType() {
        Intent i = getIntent();
        if (i == null || i.getData() == null) {
            return -1;
        }
        return i.getIntExtra("type", -1);
    }

}
