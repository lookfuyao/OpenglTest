package com.fy.opengltest.FourBlockCalibrate;

import android.content.Context;
import android.graphics.Point;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by yao.fu on 17-1-23.
 */
public class GLRendererBlocksCilibration implements GLSurfaceView.Renderer {

    private static final int DISPLAY_SIZE = 4;
    private Context mContext;
    private List<Uri> mUris;

    private List<FishCalibrateObject> objectList;

    public GLRendererBlocksCilibration(Context mContext, List<Uri> mUris) {
        this.mContext = mContext;
        this.mUris = mUris;
    }

    private void initObjects(){
        if (null == objectList){
            objectList = new ArrayList<>();
        }
        for(int i = 0; i < DISPLAY_SIZE; i++){
            FishCalibrateObject object = new FishCalibrateObject(mContext, mUris.get(i));
            object.init();
            objectList.add(object);
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        initObjects();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        ArrayList<Point> mPoss = new ArrayList<>(DISPLAY_SIZE);
        ArrayList<Point> mSizes = new ArrayList<>(DISPLAY_SIZE);
        mPoss.add(new Point(0, height / 2));
        mPoss.add(new Point(width / 2, height / 2));
        mPoss.add(new Point(0, 0));
        mPoss.add(new Point(width / 2, 0));
        mSizes.add(new Point(width / 2, height / 2));
        mSizes.add(new Point(width / 2, height / 2));
        mSizes.add(new Point(width / 2, height / 2));
        mSizes.add(new Point(width / 2, height / 2));
        for(int i = 0; i < DISPLAY_SIZE; i++) {
            FishCalibrateObject object = objectList.get(i);
            object.updateViewport(mPoss.get(i), mSizes.get(i));
        }
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);
        for(int i = 0; i < DISPLAY_SIZE; i++) {
            FishCalibrateObject object = objectList.get(i);
            object.draw();
        }
    }
}
