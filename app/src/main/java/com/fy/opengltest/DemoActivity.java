package com.fy.opengltest;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.fy.opengltest.renderSphere.ActivitySphere;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.fy.opengltest.MainActivity.TYPE_Sphere;
import static com.fy.opengltest.MainActivity.TYPE_multi_Sphere;
import static com.fy.opengltest.OpenglApplication.TEST_PIC_PATH;

/**
 * Created by hzqiujiadi on 16/1/26.
 * hzqiujiadi ashqalcn@gmail.com
 */
public class DemoActivity extends AppCompatActivity {

    public static final String sPath = "file:///mnt/sdcard/vr/";

    private SpinnerHelper spinnerHelper;

    private EditText mEditText;

    private Uri mSelectUri = null;

    private int mRenderType = 1;

    private static final String TAG = DemoActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        mEditText = (EditText) findViewById(R.id.edit_text_url);

        SparseArray<String> data = new SparseArray<>();

        data.put(data.size(), sPath + "dome190_24402400.jpg");

        spinnerHelper = SpinnerHelper.with(this)
                .setData(data)
                .setClickHandler(new SpinnerHelper.ClickHandler() {
                    @Override
                    public void onSpinnerClicked(int index, int key, String value) {
                        mEditText.setText(value);
                        mSelectUri = null;
                    }
                });
        spinnerHelper.init(R.id.spinner_url);

        SparseArray<String> types = new SparseArray<>();
        types.put(MainActivity.TYPE_TRIANGLE, "Triangle");
        types.put(MainActivity.TYPE_Quatrangle, "Quatrangle");
        types.put(MainActivity.TYPE_PICTURE, "Picture");
        types.put(MainActivity.TYPE_CIRCLE_PICTURE, "Circle Picture");
        types.put(MainActivity.TYPE_CIRCLE, "Circle");
        types.put(MainActivity.TYPE_CIRCLE_2_RECTANGLE, "Circle2Rectangle");
        types.put(TYPE_Sphere, "Sphere");
        types.put(TYPE_multi_Sphere, "Multi Sphere");
        types.put(MainActivity.TYPE_FISH_TO_QUANJING, "Fish to Quanjing");
        types.put(MainActivity.TYPE_FishCilibration, "Fish Cilibration");
        types.put(MainActivity.TYPE_FISH_4_BLOCK_Calibrate, "Four block Fish Cilibration");
        SpinnerHelper.with(this)
                .setData(types)
                .setDefault(MainActivity.TYPE_CIRCLE_2_RECTANGLE)
                .setClickHandler(new SpinnerHelper.ClickHandler() {
                    @Override
                    public void onSpinnerClicked(int index, int key, String value) {
                        mRenderType = key;
                    }
                }).init(R.id.render_type);

        findViewById(R.id.video_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = mEditText.getText().toString();
                if (!TextUtils.isEmpty(url)) {
                    if (mRenderType == TYPE_Sphere) {
                        Class<? extends Activity> clz = ActivitySphere.class;
                        MainActivity.start(DemoActivity.this, Uri.parse(url), clz, mRenderType);
                    } else if (mRenderType == TYPE_multi_Sphere) {
                        Class<? extends Activity> clz = ActivityCar360Sphere.class;
                        MainActivity.start(DemoActivity.this, Uri.parse(url), clz, mRenderType);
                    } else
                        MainActivity.start(DemoActivity.this, Uri.parse(url), MainActivity.class, mRenderType);
                } else {
                    Toast.makeText(DemoActivity.this, "empty url!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.choose_file).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("*/*");
                startActivityForResult(intent, 3);
            }
        });
        initFile();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 3) {
            mSelectUri = data.getData();
            mEditText.setText(mSelectUri.toString());
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private Uri getDrawableUri(@DrawableRes int resId) {
        Resources resources = getResources();
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resId) + '/' + resources.getResourceTypeName(resId) + '/' + resources.getResourceEntryName(resId));
    }

    private void initFile() {
        final SparseArray<String> data = new SparseArray<>();
        List<File> files = new ArrayList<>();
        getFiles(TEST_PIC_PATH, files);
        for (File file : files) {
            if (file.getPath().endsWith("png") || file.getPath().endsWith("bmp") || file.getPath().endsWith("jpg"))
                data.put(data.size(), Uri.fromFile(file).toString());
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                spinnerHelper.updateData(data, R.id.spinner_url);
            }
        });
    }

    void getFiles(String parentPath, List<File> fileList) {
        File dir = new File(parentPath);
        final File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                fileList.add(file);
            } else {
                getFiles(file.getPath(), fileList);
            }
        }
    }

}
